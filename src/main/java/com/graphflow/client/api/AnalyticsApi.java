package com.graphflow.client.api;

import com.graphflow.client.ApiException;
import com.graphflow.client.ApiInvoker;
import com.graphflow.client.model.UserConversionPerformance;
import com.graphflow.client.model.Interaction;
import com.graphflow.client.model.UserPerformance;
import com.graphflow.client.model.ItemPerformance;
import com.graphflow.client.model.UserSearchResult;
import com.graphflow.client.model.ItemConversionPerformance;
import com.graphflow.client.model.ItemSearchResult;
import com.graphflow.client.model.ItemAttributeResult;
import java.util.*;

public class AnalyticsApi {
  String basePath = "https://api-dev.graphflow.com";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }
  
  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }
  
  public String getBasePath() {
    return basePath;
  }

  public Map<String, Double> getABTestSummary (String fromTime, String toTime, String userId, String itemId, Boolean uniqueUsers) throws ApiException {
    // create path and map variables
    String path = "/analytics/interaction/abtest".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(uniqueUsers)))
      queryParams.put("uniqueUsers", String.valueOf(uniqueUsers));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Map<String, Double>) ApiInvoker.deserialize(response, "Map", Map.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Map<String, Double> getInteractionCount (String fromTime, String toTime, String userId, String itemId) throws ApiException {
    // create path and map variables
    String path = "/analytics/interaction/count".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Map<String, Double>) ApiInvoker.deserialize(response, "Map", Map.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<Interaction> getInteractionSearch (String q, Integer num, String fromTime, String toTime, String userId, String itemId, Boolean withDetails) throws ApiException {
    // create path and map variables
    String path = "/analytics/interaction/search".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<Interaction>) ApiInvoker.deserialize(response, "List", Interaction.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Map<String, Double> getInteractionTime (String fromTime, String toTime, String userId, String itemId, String groupByKey, Boolean uniqueUsers, Boolean uniqueItems, String interval) throws ApiException {
    // create path and map variables
    String path = "/analytics/interaction/time".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(groupByKey)))
      queryParams.put("groupByKey", String.valueOf(groupByKey));
    if(!"null".equals(String.valueOf(uniqueUsers)))
      queryParams.put("uniqueUsers", String.valueOf(uniqueUsers));
    if(!"null".equals(String.valueOf(uniqueItems)))
      queryParams.put("uniqueItems", String.valueOf(uniqueItems));
    if(!"null".equals(String.valueOf(interval)))
      queryParams.put("interval", String.valueOf(interval));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Map<String, Double>) ApiInvoker.deserialize(response, "Map", Map.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<ItemPerformance> getItemActivity (String type, String fromTime, String toTime, Integer num, Boolean withDetails, String fields, Boolean reverse, Boolean revenue, Boolean trending) throws ApiException {
    // create path and map variables
    String path = "/analytics/item/activity".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(type)))
      queryParams.put("type", String.valueOf(type));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(reverse)))
      queryParams.put("reverse", String.valueOf(reverse));
    if(!"null".equals(String.valueOf(revenue)))
      queryParams.put("revenue", String.valueOf(revenue));
    if(!"null".equals(String.valueOf(trending)))
      queryParams.put("trending", String.valueOf(trending));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<ItemPerformance>) ApiInvoker.deserialize(response, "List", ItemPerformance.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<ItemConversionPerformance> getItemConversion (String fromType, String toType, Integer minFrom, Integer minTo, String fromTime, String toTime, Integer num, Boolean withDetails, String fields, Boolean orderFrom, Boolean reverseSortType, Boolean reverseSortRatio) throws ApiException {
    // create path and map variables
    String path = "/analytics/item/conversion".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(fromType == null || toType == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(fromType)))
      queryParams.put("fromType", String.valueOf(fromType));
    if(!"null".equals(String.valueOf(toType)))
      queryParams.put("toType", String.valueOf(toType));
    if(!"null".equals(String.valueOf(minFrom)))
      queryParams.put("minFrom", String.valueOf(minFrom));
    if(!"null".equals(String.valueOf(minTo)))
      queryParams.put("minTo", String.valueOf(minTo));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(orderFrom)))
      queryParams.put("orderFrom", String.valueOf(orderFrom));
    if(!"null".equals(String.valueOf(reverseSortType)))
      queryParams.put("reverseSortType", String.valueOf(reverseSortType));
    if(!"null".equals(String.valueOf(reverseSortRatio)))
      queryParams.put("reverseSortRatio", String.valueOf(reverseSortRatio));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<ItemConversionPerformance>) ApiInvoker.deserialize(response, "List", ItemConversionPerformance.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Long getItemCount (String q, String fromTime, String toTime) throws ApiException {
    // create path and map variables
    String path = "/analytics/item/count".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Long) ApiInvoker.deserialize(response, "", Long.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ItemSearchResult getItemSearch (String q, String fields, String facetKey, Integer numFacets, String filters, Integer num) throws ApiException {
    // create path and map variables
    String path = "/analytics/item/search".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(facetKey)))
      queryParams.put("facetKey", String.valueOf(facetKey));
    if(!"null".equals(String.valueOf(numFacets)))
      queryParams.put("numFacets", String.valueOf(numFacets));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ItemSearchResult) ApiInvoker.deserialize(response, "", ItemSearchResult.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<ItemAttributeResult> getItemSearchTrending (String field, String q, String fromTime, String toTime, Integer num, Integer minItems) throws ApiException {
    // create path and map variables
    String path = "/analytics/item/search/trending".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(field == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(field)))
      queryParams.put("field", String.valueOf(field));
    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(minItems)))
      queryParams.put("minItems", String.valueOf(minItems));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<ItemAttributeResult>) ApiInvoker.deserialize(response, "List", ItemAttributeResult.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Map<String, Double> getRecservedCount (String fromTime, String toTime, String userId, String itemId, String groupByKey) throws ApiException {
    // create path and map variables
    String path = "/analytics/recserved/count".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(groupByKey)))
      queryParams.put("groupByKey", String.valueOf(groupByKey));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Map<String, Double>) ApiInvoker.deserialize(response, "Map", Map.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Map<String, Double> getRecservedTime (String fromTime, String toTime, String userId, String itemId, String groupByKey, Boolean uniqueUsers, Boolean uniqueItems, String interval) throws ApiException {
    // create path and map variables
    String path = "/analytics/recserved/time".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(groupByKey)))
      queryParams.put("groupByKey", String.valueOf(groupByKey));
    if(!"null".equals(String.valueOf(uniqueUsers)))
      queryParams.put("uniqueUsers", String.valueOf(uniqueUsers));
    if(!"null".equals(String.valueOf(uniqueItems)))
      queryParams.put("uniqueItems", String.valueOf(uniqueItems));
    if(!"null".equals(String.valueOf(interval)))
      queryParams.put("interval", String.valueOf(interval));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Map<String, Double>) ApiInvoker.deserialize(response, "Map", Map.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<UserPerformance> getUserActivity (String type, String fromTime, String toTime, Integer num, Boolean withDetails, String fields, Boolean reverse, Boolean revenue) throws ApiException {
    // create path and map variables
    String path = "/analytics/user/activity".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(type)))
      queryParams.put("type", String.valueOf(type));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(reverse)))
      queryParams.put("reverse", String.valueOf(reverse));
    if(!"null".equals(String.valueOf(revenue)))
      queryParams.put("revenue", String.valueOf(revenue));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<UserPerformance>) ApiInvoker.deserialize(response, "List", UserPerformance.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<UserConversionPerformance> getUserConversion (String fromType, String toType, Integer minFrom, Integer minTo, String fromTime, String toTime, Integer num, Boolean withDetails, String fields, Boolean orderFrom, Boolean reverseSortType, Boolean reverseSortRatio) throws ApiException {
    // create path and map variables
    String path = "/analytics/user/conversion".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(fromType == null || toType == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(fromType)))
      queryParams.put("fromType", String.valueOf(fromType));
    if(!"null".equals(String.valueOf(toType)))
      queryParams.put("toType", String.valueOf(toType));
    if(!"null".equals(String.valueOf(minFrom)))
      queryParams.put("minFrom", String.valueOf(minFrom));
    if(!"null".equals(String.valueOf(minTo)))
      queryParams.put("minTo", String.valueOf(minTo));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(orderFrom)))
      queryParams.put("orderFrom", String.valueOf(orderFrom));
    if(!"null".equals(String.valueOf(reverseSortType)))
      queryParams.put("reverseSortType", String.valueOf(reverseSortType));
    if(!"null".equals(String.valueOf(reverseSortRatio)))
      queryParams.put("reverseSortRatio", String.valueOf(reverseSortRatio));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<UserConversionPerformance>) ApiInvoker.deserialize(response, "List", UserConversionPerformance.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Long getUserCount (String q, String fromTime, String toTime) throws ApiException {
    // create path and map variables
    String path = "/analytics/user/count".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Long) ApiInvoker.deserialize(response, "", Long.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public UserSearchResult getUserSearch (String q, String fields, String facetKey, Integer numFacets, String filters, Integer num) throws ApiException {
    // create path and map variables
    String path = "/analytics/user/search".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    if(!"null".equals(String.valueOf(q)))
      queryParams.put("q", String.valueOf(q));
    if(!"null".equals(String.valueOf(fields)))
      queryParams.put("fields", String.valueOf(fields));
    if(!"null".equals(String.valueOf(facetKey)))
      queryParams.put("facetKey", String.valueOf(facetKey));
    if(!"null".equals(String.valueOf(numFacets)))
      queryParams.put("numFacets", String.valueOf(numFacets));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (UserSearchResult) ApiInvoker.deserialize(response, "", UserSearchResult.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  }

