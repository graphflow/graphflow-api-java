package com.graphflow.client.api;

import com.graphflow.client.ApiException;
import com.graphflow.client.ApiInvoker;
import java.util.*;
import com.graphflow.client.model.Interaction;
import com.graphflow.client.model.Item;
import com.graphflow.client.model.ApiResponse;
import java.util.*;

public class ItemApi {
  String basePath = "https://api-dev.graphflow.com";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }
  
  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }
  
  public String getBasePath() {
    return basePath;
  }

  public ApiResponse putItem (Item body) throws ApiException {
    // create path and map variables
    String path = "/item/".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse putItemActiveToggle (String itemId, Boolean active) throws ApiException {
    // create path and map variables
    String path = "/item/activetoggle".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(itemId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(active)))
      queryParams.put("active", String.valueOf(active));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, null, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<Interaction> getInteractions (String itemId, String type, Integer num, Boolean withDetails) throws ApiException {
    // create path and map variables
    String path = "/item/interaction".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(itemId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(type)))
      queryParams.put("type", String.valueOf(type));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<Interaction>) ApiInvoker.deserialize(response, "List", Interaction.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse putItemList (List<Item> body) throws ApiException {
    // create path and map variables
    String path = "/item/itemlist".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public Item getItemById (String itemId) throws ApiException {
    // create path and map variables
    String path = "/item/{itemId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "itemId" + "\\}", apiInvoker.escapeString(itemId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(itemId == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (Item) ApiInvoker.deserialize(response, "", Item.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse deleteItemById (String itemId, Boolean hardDelete) throws ApiException {
    // create path and map variables
    String path = "/item/{itemId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "itemId" + "\\}", apiInvoker.escapeString(itemId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(itemId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(hardDelete)))
      queryParams.put("hardDelete", String.valueOf(hardDelete));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "DELETE", queryParams, null, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  }

