package com.graphflow.client.api;

import com.graphflow.client.ApiException;
import com.graphflow.client.ApiInvoker;
import com.graphflow.client.model.ItemRecommendationResponse;
import java.util.*;

public class RecommendApi {
  String basePath = "https://api-dev.graphflow.com";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }
  
  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }
  
  public String getBasePath() {
    return basePath;
  }

  public ItemRecommendationResponse getPopularItems (String userId, String type, String fromTime, String toTime, Integer num, Boolean itemDetails, Boolean excludeConverted, String filters, String recType, String aggType) throws ApiException {
    // create path and map variables
    String path = "/recommend/item/popular".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(type)))
      queryParams.put("type", String.valueOf(type));
    if(!"null".equals(String.valueOf(fromTime)))
      queryParams.put("fromTime", String.valueOf(fromTime));
    if(!"null".equals(String.valueOf(toTime)))
      queryParams.put("toTime", String.valueOf(toTime));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(itemDetails)))
      queryParams.put("itemDetails", String.valueOf(itemDetails));
    if(!"null".equals(String.valueOf(excludeConverted)))
      queryParams.put("excludeConverted", String.valueOf(excludeConverted));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(recType)))
      queryParams.put("recType", String.valueOf(recType));
    if(!"null".equals(String.valueOf(aggType)))
      queryParams.put("aggType", String.valueOf(aggType));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ItemRecommendationResponse) ApiInvoker.deserialize(response, "", ItemRecommendationResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ItemRecommendationResponse getSimilarItems (String itemId, String userId, Integer num, Boolean itemDetails, Boolean excludeConverted, Boolean purchasedTogether, String filters, String recType) throws ApiException {
    // create path and map variables
    String path = "/recommend/item/similar".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(itemId == null || userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(itemDetails)))
      queryParams.put("itemDetails", String.valueOf(itemDetails));
    if(!"null".equals(String.valueOf(excludeConverted)))
      queryParams.put("excludeConverted", String.valueOf(excludeConverted));
    if(!"null".equals(String.valueOf(purchasedTogether)))
      queryParams.put("purchasedTogether", String.valueOf(purchasedTogether));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(recType)))
      queryParams.put("recType", String.valueOf(recType));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ItemRecommendationResponse) ApiInvoker.deserialize(response, "", ItemRecommendationResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ItemRecommendationResponse getRecommendations (String userId, Integer num, Boolean itemDetails, Boolean excludeConverted, Boolean purchasedTogether, String filters, String recType) throws ApiException {
    // create path and map variables
    String path = "/recommend/user/{userId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "userId" + "\\}", apiInvoker.escapeString(userId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(itemDetails)))
      queryParams.put("itemDetails", String.valueOf(itemDetails));
    if(!"null".equals(String.valueOf(excludeConverted)))
      queryParams.put("excludeConverted", String.valueOf(excludeConverted));
    if(!"null".equals(String.valueOf(purchasedTogether)))
      queryParams.put("purchasedTogether", String.valueOf(purchasedTogether));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(recType)))
      queryParams.put("recType", String.valueOf(recType));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ItemRecommendationResponse) ApiInvoker.deserialize(response, "", ItemRecommendationResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ItemRecommendationResponse getRecommendationsUserItem (String userId, String itemId, Integer num, Boolean itemDetails, Boolean excludeConverted, Boolean purchasedTogether, String filters, String recType) throws ApiException {
    // create path and map variables
    String path = "/recommend/useritem/{userId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "userId" + "\\}", apiInvoker.escapeString(userId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null || itemId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(itemId)))
      queryParams.put("itemId", String.valueOf(itemId));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(itemDetails)))
      queryParams.put("itemDetails", String.valueOf(itemDetails));
    if(!"null".equals(String.valueOf(excludeConverted)))
      queryParams.put("excludeConverted", String.valueOf(excludeConverted));
    if(!"null".equals(String.valueOf(purchasedTogether)))
      queryParams.put("purchasedTogether", String.valueOf(purchasedTogether));
    if(!"null".equals(String.valueOf(filters)))
      queryParams.put("filters", String.valueOf(filters));
    if(!"null".equals(String.valueOf(recType)))
      queryParams.put("recType", String.valueOf(recType));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ItemRecommendationResponse) ApiInvoker.deserialize(response, "", ItemRecommendationResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  }

