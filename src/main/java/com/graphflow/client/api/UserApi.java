package com.graphflow.client.api;

import com.graphflow.client.ApiException;
import com.graphflow.client.ApiInvoker;
import java.util.*;
import com.graphflow.client.model.User;
import com.graphflow.client.model.PostedInteraction;
import com.graphflow.client.model.Interaction;
import com.graphflow.client.model.ApiResponse;
import java.util.*;

public class UserApi {
  String basePath = "https://api-dev.graphflow.com";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }
  
  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }
  
  public String getBasePath() {
    return basePath;
  }

  public ApiResponse putUser (User body) throws ApiException {
    // create path and map variables
    String path = "/user/".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse aliasUser (String userId, String otherId) throws ApiException {
    // create path and map variables
    String path = "/user/alias".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null || otherId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(otherId)))
      queryParams.put("otherId", String.valueOf(otherId));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public List<Interaction> getInteractions (String userId, String type, Integer num, Boolean withDetails) throws ApiException {
    // create path and map variables
    String path = "/user/interaction".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(userId)))
      queryParams.put("userId", String.valueOf(userId));
    if(!"null".equals(String.valueOf(type)))
      queryParams.put("type", String.valueOf(type));
    if(!"null".equals(String.valueOf(num)))
      queryParams.put("num", String.valueOf(num));
    if(!"null".equals(String.valueOf(withDetails)))
      queryParams.put("withDetails", String.valueOf(withDetails));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (List<Interaction>) ApiInvoker.deserialize(response, "List", Interaction.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse postInteraction (PostedInteraction body) throws ApiException {
    // create path and map variables
    String path = "/user/interaction".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "POST", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse postInteractionList (List<PostedInteraction> body) throws ApiException {
    // create path and map variables
    String path = "/user/interactionlist".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "POST", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse putUserList (List<User> body) throws ApiException {
    // create path and map variables
    String path = "/user/userlist".replaceAll("\\{format\\}","json");

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(body == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, body, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public User getUserById (String userId) throws ApiException {
    // create path and map variables
    String path = "/user/{userId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "userId" + "\\}", apiInvoker.escapeString(userId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, null, headerParams);
      if(response != null){
        return (User) ApiInvoker.deserialize(response, "", User.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  public ApiResponse deleteUserById (String userId, Boolean hardDelete) throws ApiException {
    // create path and map variables
    String path = "/user/{userId}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "userId" + "\\}", apiInvoker.escapeString(userId.toString()));

    // query params
    Map<String, String> queryParams = new HashMap<String, String>();
    Map<String, String> headerParams = new HashMap<String, String>();

    // verify required params are set
    if(userId == null ) {
       throw new ApiException(400, "missing required params");
    }
    if(!"null".equals(String.valueOf(hardDelete)))
      queryParams.put("hardDelete", String.valueOf(hardDelete));
    try {
      String response = apiInvoker.invokeAPI(basePath, path, "DELETE", queryParams, null, headerParams);
      if(response != null){
        return (ApiResponse) ApiInvoker.deserialize(response, "", ApiResponse.class);
      }
      else {
        return null;
      }
    } catch (ApiException ex) {
      if(ex.getCode() == 404) {
      	return null;
      }
      else {
        throw ex;
      }
    }
  }
  }

