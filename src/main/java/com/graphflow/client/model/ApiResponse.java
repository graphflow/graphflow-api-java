package com.graphflow.client.model;

public class ApiResponse {
  /*  */
  private String result = null;
  /*  */
  private String message = null;
  /*  */
  private String code = null;
  public String getResult() {
    return result;
  }
  public void setResult(String result) {
    this.result = result;
  }

  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiResponse {\n");
    sb.append("  result: ").append(result).append("\n");
    sb.append("  message: ").append(message).append("\n");
    sb.append("  code: ").append(code).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

