package com.graphflow.client.model;

import java.util.Map;

public class Interaction {
  /*  */
  private Double weight = null;
  /*  */
  private Integer quantity = null;
  /*  */
  private Long timestamp = null;
  /*  */
  private String toId = null;
  /*  */
  private Map<String,String> interactionData = null;
  /*  */
  private Double price = null;
  /*  */
  private String interactionId = null;
  /*  */
  private String fromId = null;
  /*  */
  private Map<String,String> fromData = null;
  /*  */
  private Map<String,String> toData = null;
  /*  */
  private String interactionType = null;
  public Double getWeight() {
    return weight;
  }
  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public Integer getQuantity() {
    return quantity;
  }
  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Long getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  public String getToId() {
    return toId;
  }
  public void setToId(String toId) {
    this.toId = toId;
  }

  public Map<String,String> getInteractionData() {
    return interactionData;
  }
  public void setInteractionData(Map<String,String> interactionData) {
    this.interactionData = interactionData;
  }

  public Double getPrice() {
    return price;
  }
  public void setPrice(Double price) {
    this.price = price;
  }

  public String getInteractionId() {
    return interactionId;
  }
  public void setInteractionId(String interactionId) {
    this.interactionId = interactionId;
  }

  public String getFromId() {
    return fromId;
  }
  public void setFromId(String fromId) {
    this.fromId = fromId;
  }

  public Map<String,String> getFromData() {
    return fromData;
  }
  public void setFromData(Map<String,String> fromData) {
    this.fromData = fromData;
  }

  public Map<String,String> getToData() {
    return toData;
  }
  public void setToData(Map<String,String> toData) {
    this.toData = toData;
  }

  public String getInteractionType() {
    return interactionType;
  }
  public void setInteractionType(String interactionType) {
    this.interactionType = interactionType;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Interaction {\n");
    sb.append("  weight: ").append(weight).append("\n");
    sb.append("  quantity: ").append(quantity).append("\n");
    sb.append("  timestamp: ").append(timestamp).append("\n");
    sb.append("  toId: ").append(toId).append("\n");
    sb.append("  interactionData: ").append(interactionData).append("\n");
    sb.append("  price: ").append(price).append("\n");
    sb.append("  interactionId: ").append(interactionId).append("\n");
    sb.append("  fromId: ").append(fromId).append("\n");
    sb.append("  fromData: ").append(fromData).append("\n");
    sb.append("  toData: ").append(toData).append("\n");
    sb.append("  interactionType: ").append(interactionType).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

