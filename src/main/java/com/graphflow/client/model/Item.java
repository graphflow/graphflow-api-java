package com.graphflow.client.model;


import java.util.Map;

public class Item {
  /*  */
  private String itemId = null;
  /*  */
  private Map<String,String> itemData = null;
  /*  */
  private Object active = null;
  public String getItemId() {
    return itemId;
  }
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

  public Map<String,String> getItemData() {
    return itemData;
  }
  public void setItemData(Map<String,String> itemData) {
    this.itemData = itemData;
  }

  public Object getActive() {
    return active;
  }
  public void setActive(Object active) {
    this.active = active;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Item {\n");
    sb.append("  itemId: ").append(itemId).append("\n");
    sb.append("  itemData: ").append(itemData).append("\n");
    sb.append("  active: ").append(active).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

