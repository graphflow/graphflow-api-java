package com.graphflow.client.model;

import com.graphflow.client.model.Item;
public class ItemConversionPerformance {
  /*  */
  private Double rate = null;
  /*  */
  private Long toCount = null;
  /*  */
  private String itemId = null;
  /*  */
  private Long fromCount = null;
  /*  */
  private Item item = null;
  public Double getRate() {
    return rate;
  }
  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Long getToCount() {
    return toCount;
  }
  public void setToCount(Long toCount) {
    this.toCount = toCount;
  }

  public String getItemId() {
    return itemId;
  }
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

  public Long getFromCount() {
    return fromCount;
  }
  public void setFromCount(Long fromCount) {
    this.fromCount = fromCount;
  }

  public Item getItem() {
    return item;
  }
  public void setItem(Item item) {
    this.item = item;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemConversionPerformance {\n");
    sb.append("  rate: ").append(rate).append("\n");
    sb.append("  toCount: ").append(toCount).append("\n");
    sb.append("  itemId: ").append(itemId).append("\n");
    sb.append("  fromCount: ").append(fromCount).append("\n");
    sb.append("  item: ").append(item).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

