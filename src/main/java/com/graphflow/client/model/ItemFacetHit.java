package com.graphflow.client.model;

import java.util.*;
import com.graphflow.client.model.Item;
public class ItemFacetHit {
  /*  */
  private List<Item> hits = new ArrayList<Item>();
  /*  */
  private String facet = null;
  public List<Item> getHits() {
    return hits;
  }
  public void setHits(List<Item> hits) {
    this.hits = hits;
  }

  public String getFacet() {
    return facet;
  }
  public void setFacet(String facet) {
    this.facet = facet;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemFacetHit {\n");
    sb.append("  hits: ").append(hits).append("\n");
    sb.append("  facet: ").append(facet).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

