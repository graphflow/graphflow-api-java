package com.graphflow.client.model;

import com.graphflow.client.model.Item;
public class ItemPerformance {
  /*  */
  private Double score = null;
  /*  */
  private String itemId = null;
  /*  */
  private Double revenue = null;
  /*  */
  private Item item = null;
  public Double getScore() {
    return score;
  }
  public void setScore(Double score) {
    this.score = score;
  }

  public String getItemId() {
    return itemId;
  }
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

  public Double getRevenue() {
    return revenue;
  }
  public void setRevenue(Double revenue) {
    this.revenue = revenue;
  }

  public Item getItem() {
    return item;
  }
  public void setItem(Item item) {
    this.item = item;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemPerformance {\n");
    sb.append("  score: ").append(score).append("\n");
    sb.append("  itemId: ").append(itemId).append("\n");
    sb.append("  revenue: ").append(revenue).append("\n");
    sb.append("  item: ").append(item).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

