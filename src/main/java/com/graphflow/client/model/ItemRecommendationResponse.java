package com.graphflow.client.model;

import java.util.*;
import com.graphflow.client.model.ItemRecommendation;
public class ItemRecommendationResponse {
  /*  */
  private String recId = null;
  /*  */
  private String group = null;
  /*  */
  private List<ItemRecommendation> result = new ArrayList<ItemRecommendation>();
  /*  */
  private String code = null;
  public String getRecId() {
    return recId;
  }
  public void setRecId(String recId) {
    this.recId = recId;
  }

  public String getGroup() {
    return group;
  }
  public void setGroup(String group) {
    this.group = group;
  }

  public List<ItemRecommendation> getResult() {
    return result;
  }
  public void setResult(List<ItemRecommendation> result) {
    this.result = result;
  }

  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemRecommendationResponse {\n");
    sb.append("  recId: ").append(recId).append("\n");
    sb.append("  group: ").append(group).append("\n");
    sb.append("  result: ").append(result).append("\n");
    sb.append("  code: ").append(code).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

