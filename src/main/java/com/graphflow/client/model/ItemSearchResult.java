package com.graphflow.client.model;

import java.util.*;
import com.graphflow.client.model.Item;
import com.graphflow.client.model.ItemFacetHit;
public class ItemSearchResult {
  /*  */
  private List<Item> hits = new ArrayList<Item>();
  /*  */
  private List<ItemFacetHit> facets = new ArrayList<ItemFacetHit>();
  public List<Item> getHits() {
    return hits;
  }
  public void setHits(List<Item> hits) {
    this.hits = hits;
  }

  public List<ItemFacetHit> getFacets() {
    return facets;
  }
  public void setFacets(List<ItemFacetHit> facets) {
    this.facets = facets;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemSearchResult {\n");
    sb.append("  hits: ").append(hits).append("\n");
    sb.append("  facets: ").append(facets).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

