package com.graphflow.client.model;

import java.util.*;
public class User {
  /*  */
  private Map<String,String> userData = null;
  /*  */
  private String userId = null;
  /*  */
  private List<String> otherIds = new ArrayList<String>();
  /*  */
  private Object active = null;
  public Map<String,String> getUserData() {
    return userData;
  }
  public void setUserData(Map<String,String> userData) {
    this.userData = userData;
  }

  public String getUserId() {
    return userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public List<String> getOtherIds() {
    return otherIds;
  }
  public void setOtherIds(List<String> otherIds) {
    this.otherIds = otherIds;
  }

  public Object getActive() {
    return active;
  }
  public void setActive(Object active) {
    this.active = active;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class User {\n");
    sb.append("  userData: ").append(userData).append("\n");
    sb.append("  userId: ").append(userId).append("\n");
    sb.append("  otherIds: ").append(otherIds).append("\n");
    sb.append("  active: ").append(active).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

