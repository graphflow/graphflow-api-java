package com.graphflow.client.model;

import com.graphflow.client.model.User;
public class UserConversionPerformance {
  /*  */
  private Double rate = null;
  /*  */
  private Long toCount = null;
  /*  */
  private String userId = null;
  /*  */
  private Long fromCount = null;
  /*  */
  private User user = null;
  public Double getRate() {
    return rate;
  }
  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Long getToCount() {
    return toCount;
  }
  public void setToCount(Long toCount) {
    this.toCount = toCount;
  }

  public String getUserId() {
    return userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getFromCount() {
    return fromCount;
  }
  public void setFromCount(Long fromCount) {
    this.fromCount = fromCount;
  }

  public User getUser() {
    return user;
  }
  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserConversionPerformance {\n");
    sb.append("  rate: ").append(rate).append("\n");
    sb.append("  toCount: ").append(toCount).append("\n");
    sb.append("  userId: ").append(userId).append("\n");
    sb.append("  fromCount: ").append(fromCount).append("\n");
    sb.append("  user: ").append(user).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

