package com.graphflow.client.model;

import java.util.*;
import com.graphflow.client.model.User;
public class UserFacetHit {
  /*  */
  private List<User> hits = new ArrayList<User>();
  /*  */
  private String facet = null;
  public List<User> getHits() {
    return hits;
  }
  public void setHits(List<User> hits) {
    this.hits = hits;
  }

  public String getFacet() {
    return facet;
  }
  public void setFacet(String facet) {
    this.facet = facet;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserFacetHit {\n");
    sb.append("  hits: ").append(hits).append("\n");
    sb.append("  facet: ").append(facet).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

