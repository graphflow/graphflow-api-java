package com.graphflow.client.model;

import com.graphflow.client.model.User;
public class UserPerformance {
  /*  */
  private Double score = null;
  /*  */
  private String userId = null;
  /*  */
  private Double revenue = null;
  /*  */
  private User user = null;
  public Double getScore() {
    return score;
  }
  public void setScore(Double score) {
    this.score = score;
  }

  public String getUserId() {
    return userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Double getRevenue() {
    return revenue;
  }
  public void setRevenue(Double revenue) {
    this.revenue = revenue;
  }

  public User getUser() {
    return user;
  }
  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserPerformance {\n");
    sb.append("  score: ").append(score).append("\n");
    sb.append("  userId: ").append(userId).append("\n");
    sb.append("  revenue: ").append(revenue).append("\n");
    sb.append("  user: ").append(user).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

