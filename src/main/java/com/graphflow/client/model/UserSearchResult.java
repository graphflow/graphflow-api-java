package com.graphflow.client.model;

import java.util.*;
import com.graphflow.client.model.UserFacetHit;
import com.graphflow.client.model.User;
public class UserSearchResult {
  /*  */
  private List<User> hits = new ArrayList<User>();
  /*  */
  private List<UserFacetHit> facets = new ArrayList<UserFacetHit>();
  public List<User> getHits() {
    return hits;
  }
  public void setHits(List<User> hits) {
    this.hits = hits;
  }

  public List<UserFacetHit> getFacets() {
    return facets;
  }
  public void setFacets(List<UserFacetHit> facets) {
    this.facets = facets;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserSearchResult {\n");
    sb.append("  hits: ").append(hits).append("\n");
    sb.append("  facets: ").append(facets).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

